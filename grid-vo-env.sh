if [ "X${GRID_VO_ENV_SET+X}" = "X" ]; then
    . /usr/libexec/grid-env-funcs.sh
    if [ "x${GLITE_UI_ARCH:-$1}" = "x32BIT" ]; then arch_dir=lib; else arch_dir=lib64; fi

    # specific settings for vo.aleph.cern.ch
    gridenv_set         "VO_VO_ALEPH_CERN_CH_SW_DIR" "/afs/cern.ch/aleph"
    gridenv_set         "VO_VO_ALEPH_CERN_CH_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for alice
    gridenv_set         "VO_ALICE_SW_DIR" "/afs/cern.ch/project/gd/apps/alice"
    gridenv_set         "VO_ALICE_DEFAULT_SE" "srm-alice.cern.ch"

    # specific settings for atlas
    gridenv_set         "VO_ATLAS_SW_DIR" "/cvmfs/atlas.cern.ch/repo/sw"
    gridenv_set         "VO_ATLAS_DEFAULT_SE" "srm-atlas.cern.ch"

    # specific settings for cms
    gridenv_set         "VO_CMS_SW_DIR" "/cvmfs/cms.cern.ch"
    gridenv_set         "VO_CMS_DEFAULT_SE" "srm-cms.cern.ch"

    # specific settings for vo.delphi.cern.ch
    gridenv_set         "VO_VO_DELPHI_CERN_CH_SW_DIR" "/afs/cern.ch/delphi"
    gridenv_set         "VO_VO_DELPHI_CERN_CH_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for dteam
    gridenv_set         "VO_DTEAM_SW_DIR" "/afs/cern.ch/project/gd/apps/dteam"
    gridenv_set         "VO_DTEAM_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for geant4
    gridenv_set         "VO_GEANT4_SW_DIR" "/afs/cern.ch/project/gd/apps/geant4"
    gridenv_set         "VO_GEANT4_DEFAULT_SE" "lxdpm101.cern.ch"

    # specific settings for vo.gear.cern.ch
    gridenv_set         "VO_VO_GEAR_CERN_CH_SW_DIR" "/afs/cern.ch/project/gd/apps/gear"
    gridenv_set         "VO_VO_GEAR_CERN_CH_DEFAULT_SE" "lxdpm104.cern.ch"

    # specific settings for ilc
    gridenv_set         "VO_ILC_SW_DIR" "/cvmfs/ilc.desy.de"
    gridenv_set         "VO_ILC_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for vo.l3.cern.ch
    gridenv_set         "VO_VO_L3_CERN_CH_SW_DIR" "/afs/cern.ch/l3"
    gridenv_set         "VO_VO_L3_CERN_CH_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for lhcb
    gridenv_set         "VO_LHCB_SW_DIR" "/cvmfs/lhcb.cern.ch"
    gridenv_set         "VO_LHCB_DEFAULT_SE" "srm-lhcb.cern.ch"

    # specific settings for na48
    gridenv_set         "VO_NA48_SW_DIR" "/afs/cern.ch/project/gd/apps/na48"
    gridenv_set         "VO_NA48_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for vo.opal.cern.ch
    gridenv_set         "VO_VO_OPAL_CERN_CH_SW_DIR" "/afs/cern.ch/opal"
    gridenv_set         "VO_VO_OPAL_CERN_CH_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for ops
    gridenv_set         "VO_OPS_SW_DIR" "/afs/cern.ch/project/gd/apps/ops"
    gridenv_set         "VO_OPS_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for vo.sixt.cern.ch
    gridenv_set         "VO_VO_SIXT_CERN_CH_SW_DIR" "/afs/cern.ch/project/gd/apps/sixt"
    gridenv_set         "VO_VO_SIXT_CERN_CH_DEFAULT_SE" "srm-public.cern.ch"

    # specific settings for unosat
    gridenv_set         "VO_UNOSAT_SW_DIR" "/afs/cern.ch/project/gd/apps/unosat"
    gridenv_set         "VO_UNOSAT_DEFAULT_SE" "srm-public.cern.ch"
    gridenv_set         "GRID_VO_ENV_SET" "true"
    . /usr/libexec/clean-grid-env-funcs.sh
fi

